
# PROJECT Component Repository

TODO: Add a description about this component.

## Getting Started

### Install

Simply include the latest version in your `.gitlab-ci.yml` configuration.

```yaml
# .gitlab-ci.yml

include:
  - component: gitlab.com/ci-components/PROJECT_NAME@VERSION
```

> https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration

#### Versions
```yaml
# .gitlab-ci.yml

include:
  # Latest release
  - component: gitlab.com/ci-components/PROJECT_NAME@~latest
    inputs:
      job-name: 'latest'
  # Tagged version
  - component: gitlab.com/ci-components/PROJECT_NAME@v1.0
  # Branch
  - component: gitlab.com/ci-components/PROJECT_NAME@main
```

### Inputs

| Key | Type | Default | Description |
| --- | --- | --- | --- |
| `job-name` | `string` | `project` | The job name to run the included component as. |
| `job-stage` | `string` | `test` | The stage for the component to run. |
| `job-image` | `string` | `ruby:latest` | The image to use to run the component with. |

> https://docs.gitlab.com/ee/ci/yaml/inputs.html#set-input-values-when-using-include

## Versioning

This project aims to follow [semantic versioning](https://semver.org/):

> Given a version number MAJOR.MINOR.PATCH, increment the:
>
> 1. MAJOR version when you make incompatible API changes
> 1. MINOR version when you add functionality in a backward compatible manner
> 1. PATCH version when you make backward compatible bug fixes
> 1. Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

## Credits

## License